## Lil Ruby Rspec Test

Gitlab has cool features where it runs your tests for you, this is a simple example of that using rspec.

The test results need to be in junit xml format. In order to generate the test results that way, run the command below.

	rspec spec/spec001.rb --format RspecJunitFormatter --out reports/rspec.xml
