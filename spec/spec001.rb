describe 'doing a cool thing' do
    it 'adds two numbers' do
        expect(10 + 60).to eql(70)
    end
end

describe 'doing another cool thing' do
     it 'adds two numbers' do
         expect(1 + 6).to eql(7)
     end
end
