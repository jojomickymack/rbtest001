describe 'doing stuff 3' do
    it 'adds two numbers' do
        expect(10 + 60).to eql(70)
    end
end

describe 'do stuff 4' do
     it 'adds two numbers again' do
         expect(1 + 6).to eql(7)
     end
end
